def vowel_swapper(string):
    # ==============
    newnew = ''

    for char in string:
            if char not in 'aeiouAEIOU':
                newnew = newnew + char
            
            if char == 'a':
                newnew = newnew + '4'
            
            if char == 'A':
                newnew = newnew + '4'
           
            if char == ' ':
                newnew = newnew + ' '

            if char == 'e':
                newnew = newnew + '3'

            if char == 'E':
                newnew = newnew + '3'

            if char == 'i':
                newnew = newnew + '!'

            if char == 'I':
                newnew = newnew + '!'

            if char == 'o':
                newnew = newnew + 'ooo'

            if char == 'O':
                newnew = newnew + '000'

            if char == 'u':
                newnew = newnew + '|_|'

            if char == 'U':
                newnew = newnew + '|_|'        
        
    return newnew          
    
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console